package Homework04;

import java.time.DayOfWeek;
import java.util.Arrays;

import static Homework04.Constants.*;

public class Schedule {
    private String[][] scheduleItems;
    Schedule() {
        scheduleItems = Arrays.stream(DayOfWeek.values())
            .map(dayOfWeek -> new String[]{dayOfWeek.toString(), ""})
            .toArray(String[][]::new);
    }
    @Override
    public int hashCode() {
        Integer hashcode = 1;
        for (int i = 0; i < scheduleItems.length; i++) {
            for (int j = 0; j < scheduleItems[i].length; j++) {
                hashcode += scheduleItems[i][j].hashCode();
            }
        }
        return hashcode;
    }
    @Override
    public boolean equals(Object that) {
        if (that == null) return false;
        if (that == this) return true;
        if (!(that.getClass().equals(this.getClass()))) return false;
        if (this.hashCode() != that.hashCode()) return false;
        Schedule _that = (Schedule) that;
        for (int i = 0; i < scheduleItems.length; i++) {
            for (int j = 0; j < scheduleItems[i].length; j++) {
                if (!scheduleItems[i][j].equals(_that.scheduleItems[i][j])) return false;
            }
        }
        return true;
    }
    public String[][] getScheduleItems() {
        return scheduleItems;
    }
    public String getDayTasks(DayOfWeek dayOfWeek) {
        String[] scheduleItem = Arrays.stream(scheduleItems)
            .filter(item -> item[dayOfWeekKey] == dayOfWeek.toString())
            .findFirst()
            .orElse(null);
        return scheduleItem != null ? scheduleItem[tasksKey] : "";
    }
    public void updateDayTasks(DayOfWeek dayOfWeek, String tasks) {
        String[][] updatedScheduleItems = Arrays.stream(scheduleItems)
            .map(scheduleItem -> scheduleItem[dayOfWeekKey] != dayOfWeek.toString()
                ? scheduleItem
                : new String[]{scheduleItem[dayOfWeekKey], tasks})
            .toArray(String[][]::new);
        scheduleItems = updatedScheduleItems;
    }
}
