package Homework04;

import java.util.stream.IntStream;

public class Family {
    private final Human mother;

    private final Human father;

    private Human[] children;

    static {
        System.out.printf("Loading class %s\n", Family.class.getName());
    }

    {
        System.out.printf("New object of type %s is created\n", this.getClass());
    }

    Family(
        Human _mother,
        Human _father,
        Human[] _children
    ) {
        mother = _mother;
        father = _father;
        children = _children;
        mother.setFamily(this);
        father.setFamily(this);
    }

    Family(
        Human mother,
        Human father
    ) {
       this(mother, father, new Human[]{});
    }

    @Override
    public String toString() {
        StringBuilder childrenSb = new StringBuilder();
        childrenSb.append("[");
        for (int i = 0; i < children.length; i++) {
            if (i > 0) childrenSb.append(", ");
            childrenSb.append(children[i].toString());
        }
        childrenSb.append("]");
        String childrenString = childrenSb.toString();
        return String.format("Family{father=%s, mother=%s, children=%s, pet=%s",
                father.toString(),
                mother.toString(),
                childrenString
        );
    }
    @Override
    public int hashCode() {
        Integer hashcode = mother.hashCode()
                + father.hashCode();
        for (Human child : children) {
            hashcode += child.hashCode();
        }
        return hashcode;
    }
    @Override
    public boolean equals(Object that) {
        if (that == null) return false;
        if (that == this) return true;
        if (!(that.getClass().equals(this.getClass()))) return false;
        if (this.hashCode() != that.hashCode()) return false;
        Family _that = (Family) that;
        for (int i = 0; i < children.length; i++) {
            if (!children[i].equals(_that.children[i])) return false;
        }
        return mother.equals(_that.mother)
            && father.equals(_that.father);
    }

    public void addChild(Human child) {
        Integer updatedChildrenLength = children.length + 1;
        Human[] updatedChildren = new Human[updatedChildrenLength];
        System.arraycopy(children, 0, updatedChildren, 0, children.length);
        updatedChildren[updatedChildren.length - 1] = child;
        children = updatedChildren;
    }

    public boolean deleteChild(Integer childPos) {
        try {
            Integer updatedChildrenLength = children.length - 1;
            Human[] updatedChildren = new Human[updatedChildrenLength];
            System.arraycopy(children, childPos + 1, children, childPos, children.length);
            System.arraycopy(children, 0, updatedChildren, 0, updatedChildrenLength);
            children = updatedChildren;
            return true;
        } catch (IndexOutOfBoundsException ex) {
            return false;
        }
    }

    public void deleteChild(Human child) {
        int childPos = IntStream.range(0, children.length)
                .filter(i -> children[i].equals(child))
                .findFirst()
                .orElse(-1);
        deleteChild(childPos);
    }

    public Integer countFamily() {
        return children.length + 2;
    }
}
