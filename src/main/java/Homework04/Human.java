package Homework04;

import static Homework04.Constants.dayOfWeekKey;
import static Homework04.Constants.tasksKey;
import static utils.Utils.randomInt;

public class Human {
    private final String name;
    private final String surname;
    private final Integer year;
    private final Integer iq;
    private final Schedule schedule;
    private Family family;
    private Pet pet;

    static {
        System.out.printf("Loading class %s\n", Human.class.getName());
    }

    {
        System.out.printf("New object of type %s is created\n", this.getClass());
    }

    Human(
        String _name,
        String _surname,
        Integer _year,
        Integer _iq,
        Schedule _schedule,
        Pet _pet
    ) {
        name = _name;
        surname = _surname;
        year = _year;
        iq = _iq;
        schedule = _schedule;
        pet = _pet;
    }
    Human(
        String name,
        String surname,
        Integer year
    ) {
        this(name, surname, year, 0, new Schedule(), new Pet());
    }
    Human() {
        this("", "", 0, 0, new Schedule(), new Pet());
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        String[][] scheduleItems = schedule.getScheduleItems();
        sb.append("[");
        for (int i = 0; i < scheduleItems.length; i++) {
            if (i > 0) sb.append(", ");
            String scheduleItem = String.format("[%s, %s]", scheduleItems[i][dayOfWeekKey], scheduleItems[i][tasksKey]);
            sb.append(scheduleItem);
        }
        sb.append("]");
        String habitsString = sb.toString();
        return String.format("Human{name=%s, surname=%s, year=%d, iq=%d, schedule=%s, pet=%s}",
            name,
            surname,
            year,
            iq,
            habitsString,
            pet.toString()
        );
    }

    @Override
    public int hashCode() {
        Integer hashcode = name.hashCode()
            + surname.hashCode()
            + year * 7
            + iq * 7
            + (schedule.hashCode())
            + pet.hashCode();
        return hashcode;
    }
    @Override
    public boolean equals(Object that) {
        if (that == null) return false;
        if (that == this) return true;
        if (!(that.getClass().equals(this.getClass()))) return false;
        if (this.hashCode() != that.hashCode()) return false;
        Human _that = (Human) that;
        return name.equals(_that.name)
            && surname.equals(_that.surname)
            && year.equals(_that.year)
            && iq.equals(_that.iq)
            && pet.equals(_that.pet)
            && schedule.equals(_that.schedule);
    }
    public void greetPet() {
        System.out.printf("Привет, %s", pet.getNickname());
    }
    public void describePet() {
        String trickLevelToString = pet.getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый";
        System.out.printf("У меня есть %s, ему %d лет, он %s\n", pet.getNickname(), pet.getAge(), trickLevelToString);
    }
    public String getFullName() {
        return String.format("%s %s", name, surname);
    }
    public void setFamily(Family _family) {
        family = _family;
    }

    private void doFeedPet() {
        System.out.printf("Хм... покормлю ка я %s\n", this.pet.getNickname());
    }

    private void doNotFeedPet() {
        System.out.printf("Думаю, %s не голоден.\n", this.pet.getNickname());
    }

    public boolean feedPet (boolean isTimeToFeed) {
        int feedingResistLevel = randomInt(0, 100);
        boolean isFeedingDone;
        if (isTimeToFeed || this.pet.getTrickLevel() > feedingResistLevel) {
            doFeedPet();
            isFeedingDone = true;
        } else {
            doNotFeedPet();
            isFeedingDone = false;
        }
        return isFeedingDone;
    }
}
