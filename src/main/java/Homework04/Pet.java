package Homework04;

import static utils.Utils.randomInt;

public class Pet {
    private final String species;
    private final String nickname;
    private final Integer age;
    private final Integer trickLevel;
    private final String[] habits;

    static {
        System.out.printf("Loading class %s\n", Pet.class.getName());
    }

    {
        System.out.printf("New object of type %s is created\n", this.getClass().getName());
    }

    Pet(
        String _species,
        String _nickname,
        Integer _age,
        Integer _trickLevel,
        String[] _habits
    ) {
        species = _species;
        nickname = _nickname;
        age = _age;
        trickLevel = _trickLevel;
        habits = _habits;
    }
    Pet(
        String species,
        String nickname
    ) {
        this(species, nickname, 0, randomInt(0, 100), new String[]{});
    }
    Pet() {
        this("", "", 0, randomInt(0, 100), new String[]{});
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < habits.length; i++) {
            if (i > 0) sb.append(", ");
            sb.append(habits[i]);
        }
        sb.append("]");
        String habitsString = sb.toString();
        return String.format("dog{nickname='%s', age=%d, trickLevel=%d, habits=[eat, drink, sleep]}", nickname, age, trickLevel, habitsString);
    }
    @Override
    public int hashCode() {
        Integer hashcode = species.hashCode()
        + nickname.hashCode()
        + age * 7
        + trickLevel * 7;
        for (String habit : habits) {
            hashcode += habit.hashCode();
        }
        return hashcode;
    }
    @Override
    public boolean equals(Object that) {
        if (that == null) return false;
        if (that == this) return true;
        if (!(that.getClass().equals(this.getClass()))) return false;
        if (this.hashCode() != that.hashCode()) return false;
        Pet _that = (Pet) that;
        for (int i = 0; i < habits.length; i++) {
            if (!habits[i].equals(_that.habits[i])) return false;
        }
        return species.equals(_that.species)
        && nickname.equals(_that.nickname)
        && age.equals(_that.age)
        && trickLevel.equals(_that.trickLevel);
    }
    public void eat() {
        System.out.println("Я кушаю!");
    }
    public void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", nickname);
    }
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }
    public String getNickname() {
        return nickname;
    }
    public Integer getTrickLevel() {
        return trickLevel;
    }
    public Integer getAge() {
        return age;
    }
}
