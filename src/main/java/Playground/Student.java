package Playground;

public record Student(int id, String name, String group) implements IdentifiableSerializable {
}
