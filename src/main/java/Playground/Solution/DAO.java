package Playground.Solution;

import Playground.Identifiable;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;

public interface DAO<A extends Identifiable> {
    void save(A a) throws Exception;
    Optional<A> load(int id) throws Exception;
    void delete(int id) throws Exception;
    default void delete(A a) throws Exception {
        delete(a.id());
    };
}

