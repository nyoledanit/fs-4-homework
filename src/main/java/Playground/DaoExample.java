package Playground;

import Playground.Solution.DAO;
import Playground.Solution.DaoFile;
import Playground.Solution.DaoHashMap;

import java.io.File;

public class DaoExample {
    public static void main(String[] args) throws Exception {
        Pizza p1 = new Pizza(1,"Margarita", 40);
        DAO<Pizza> daoPizza = new DaoHashMap<>();
        daoPizza.save(p1);

        Student s1 = new Student(123, "Jim", "FS1");
        Student s2 = new Student(124 , "Jack", "FS1");
        File f = new File("students.bin");
        DAO<Student> daoStudents = new DaoFile<>(f);
        daoStudents.save(s1);
        daoStudents.save(s2);
        Student loaded1 = daoStudents.load(123);
        Student loaded2 = daoStudents.load(124);
        System.out.println(loaded1);
        System.out.println(loaded2);
    }
}
