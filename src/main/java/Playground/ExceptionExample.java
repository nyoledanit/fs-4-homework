package Playground;

import java.util.Optional;
import java.util.stream.Stream;

public class ExceptionExample {
    static Optional<Integer> max(int[] xs) {
        if (xs.length == 0) {
            return Optional.empty();
        }
        int max = xs[0];
        for(int x: xs) {
            if (x > max) max = x;
        }
        return Optional.of(max);
    }

    public static void main(String[] args) {
        Optional<Integer> max1 = max(new int[]{});
        Optional<Integer> max2 = max(new int[]{1, 2, 3});
        System.out.println(max1); // empty
        System.out.println(max2); // 3

        Optional<Integer> first = Stream.of(1, 2, 3)
                .filter(x -> x > 100)
                .findFirst();
    }
}


