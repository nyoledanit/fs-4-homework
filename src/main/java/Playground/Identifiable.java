package Playground;

public interface Identifiable {
    int id();
}
