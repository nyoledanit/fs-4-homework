package Playground;

import java.io.Serializable;

public interface IdentifiableSerializable extends Identifiable, Serializable {
}
