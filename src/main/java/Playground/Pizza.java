package Playground;

public record Pizza(int id, String name, int size) implements Identifiable {
}