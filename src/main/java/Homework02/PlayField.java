package Homework02;

import static utils.Utils.intToChar;

public class PlayField {
    private final Board board;
    private final int playFieldSize;
    private final char[][] playFieldState;
    private boolean isOnXAxis(int y, int x) {
        return x == 0;
    }
    private boolean isOnYAxis(int y, int x) {
        return y == 0;
    }
    private void setPlayFieldCell(int y, int x, char data) {
        playFieldState[y][x] = data;
    }
    private void setInitialPlayFieldState() {
        for (int y = 0; y < playFieldSize; y++) {
            char playFieldCell;
            for (int x = 0; x < playFieldSize; x++) {
                if (isOnXAxis(y, x)) {
                    playFieldCell = intToChar(y);
                } else if (isOnYAxis(y, x)) {
                    playFieldCell = intToChar(x);
                } else {
                    playFieldCell = '-';
                }
                setPlayFieldCell(y, x, playFieldCell);
            }
        }
    }
    private int getPlayFieldSize() {
        return board.getBoardSize() + 1;
    }
    private int coordToBoardConvert(int coord) {
        return coord - 1;
    }
    private PlayField(Board _board) {
        board = _board;
        playFieldSize = getPlayFieldSize();
        playFieldState = new char[playFieldSize][playFieldSize];
        setInitialPlayFieldState();
    }
    static PlayField of(int boardSize, int targetSize) {
        return new PlayField(Board.of(boardSize, targetSize));
    }
    public void openCell(int y, int x) {
        int boardY = coordToBoardConvert(y);
        int boardX = coordToBoardConvert(x);
        board.openCell(boardY, boardX);
    }
    public boolean isTargetHit() {
        return board.isTargetHit();
    }
    private String getFieldRepresentation() {
        StringBuilder sb = new StringBuilder();
        for (int y = 0; y < playFieldState.length; y++) {
            for (int x = 0; x < playFieldState[y].length; x++) {
                char cellRepresentation;
                if (isOnYAxis(y, x) || isOnXAxis(y, x)) {
                    cellRepresentation = playFieldState[y][x];
                } else {
                    int boardY = coordToBoardConvert(y);
                    int boardX = coordToBoardConvert(x);
                    if (!board.getCell(boardY, boardX).isActivated()) {
                        cellRepresentation = '-';
                    } else if (board.getCell(boardY, boardX).hasTarget()) {
                        cellRepresentation = 'x';
                    } else {
                        cellRepresentation = '*';
                    }
                }
                sb.append(cellRepresentation);
                sb.append("|");
            }
            sb.append("\n");
        }
        return sb.toString();
    }
    public void print() {
        String fieldRepresentation = getFieldRepresentation();
        System.out.println(fieldRepresentation);
    }
}
