package Homework02;
public record Cell (boolean hasTarget, boolean isActivated) {
    public Cell open() {
        return new Cell(hasTarget, true);
    }
}