package Homework02;

import static utils.Utils.randomInt;

public class Board {
    private final Cell[][] fieldState;
    private final Coords targetStartCoords;
    private final int boardSize;
    private final int targetSize;
    public Cell getCell(int y, int x) {
        return fieldState[y][x];
    }
    private void setCell(int y, int x, Cell cell) {
        fieldState[y][x] = cell;
    }
    private boolean isTargetCoords(int y, int x) {
        return y >= targetStartCoords.y()
            && y < (targetStartCoords.y() + targetSize)
            && x >= targetStartCoords.x()
            && x < (targetStartCoords.x() + targetSize);
    }
    private void setInitialState() {
        for (int y = 0; y < fieldState.length; y++) {
            for (int x = 0; x < fieldState[y].length; x++) {
                Cell cell;
                if (isTargetCoords(y, x)) {
                    cell = new Cell(true, false);
                } else {
                    cell = new Cell(false, false);
                }
                setCell(y, x, cell);
            }
        }
    }
    private int getMaxTargetStart() {
        return boardSize - targetSize + 1;
    }
    private Board(int _size, int _targetSize) {
        fieldState = new Cell[_size][_size];
        boardSize = _size;
        targetSize = _targetSize;
        targetStartCoords = new Coords(
            randomInt(1, getMaxTargetStart()),
            randomInt(1, getMaxTargetStart())
        );
        setInitialState();
    }
    static Board of(int size, int targetSize) {
        return new Board(size, targetSize);
    }
    public int getBoardSize() {
        return boardSize;
    }
    public boolean isTargetHit() {
        for (Cell[] row: fieldState) {
            for (Cell cell: row) {
                if (cell.hasTarget() && !cell.isActivated()) {
                    return false;
                }
            }
        }
        return true;
    }
    public void openCell(int y, int x) {
        Cell chosenCell = getCell(y, x);
        Cell openedCell = chosenCell.open();
        fieldState[y][x] = openedCell;
    }
}
