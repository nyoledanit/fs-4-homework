package Homework02;

import static utils.Utils.readIntInRange;

public class Homework02 {
    public static void main(String[] args) {
        int boardSize = 5;
        int targetSize = 3;
        PlayField playfield = PlayField.of(boardSize, targetSize);
        System.out.printf("All set. Get ready to rumble!\n");
        while (!playfield.isTargetHit()) {
            System.out.printf("Enter horizontal coordinate of target\n");
            int horizontalCoord =readIntInRange(1, boardSize);
            System.out.printf("Enter vertical coordinate of target\n");
            int verticalCoord = readIntInRange(1, boardSize);
            playfield.openCell(verticalCoord, horizontalCoord);
            playfield.print();
        }
        System.out.println("You have won!\n");
    }
}
