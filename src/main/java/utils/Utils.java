package utils;

import java.io.InputStream;
import java.util.Scanner;

public class Utils {
    public static int randomInt(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    public static boolean validateInt (String n) {
        try {
            int num = Integer.parseInt(n);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }
    private static boolean validateRange (String n, int min, int max) {
        int num = Integer.parseInt(n);
        return num >= min && num <= max;
    }

    private static boolean validateIntInRange(String n, int min, int max) {
        return validateInt(n) && validateRange(n, min, max);
    }
    public static int readIntInRange(int min, int max) {
        InputStream in = System.in;
        Scanner scanner = new Scanner(in);
        String input = scanner.nextLine();
        while (!validateIntInRange(input, min, max)) {
            System.out.printf("Entered value is invalid. Try again, enter number between %d and %d!\n", min, max);
            input = scanner.nextLine();
        }
        return Integer.parseInt(input);
    }

    public static char intToChar(int coord) {
        return (char) (coord + '0');
    }
}
