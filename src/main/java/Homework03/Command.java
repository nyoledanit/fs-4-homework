package Homework03;

public record Command(Action action, DayOfWeek dayOfWeek) {
    public Command() {
        this(null, null);
    }
}
