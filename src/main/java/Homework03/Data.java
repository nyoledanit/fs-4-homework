package Homework03;

public class Data {
    public static final String[] initialTasks = new String[] {
        "go to courses; watch a film",
        "go shopping",
        "read an article; write essay",
        "go to the park",
        "nothing to do",
        "visit friends",
        "rest on Sunday",
    };

    public static final Integer dayOfWeekKey = 0;
    public static final Integer tasksKey = 1;
}
