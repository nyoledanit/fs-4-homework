package Homework03;

public class Main {
    public static void main(String[] args) {
        Store store = Store.of();
        Schedule schedule = Schedule.of(store);
        UserInput userInput = UserInput.of();
        System.out.println("Please, input the day of the week:");
        Command command = new Command();
        while (command.action() != Action.EXIT) {
            command = userInput.readCommand();
            if (command.action() == Action.READ_TASKS) {
                String dayName = command.dayOfWeek().toString().toLowerCase();
                String tasksList = schedule.getDayTasks(command.dayOfWeek());
                System.out.printf("Your tasks for %s: %s\n", dayName, tasksList);
            }
            if (command.action() == Action.CHANGE) {
                String dayName = command.dayOfWeek().toString().toLowerCase();
                System.out.printf("Please, input new tasks for %s.\n", dayName);
                String newTasks = userInput.readNewTasks();
                schedule.updateDayTasks(command.dayOfWeek(), newTasks);
                System.out.println("Enter next action");
            }
        }
    }
}
