package Homework03;

import java.util.Arrays;

public class Utils {
    public static boolean isDayOfWeekName (String input) {
        DayOfWeek daysOfWeek = Arrays.stream(DayOfWeek.values())
            .filter(item -> item.name().equalsIgnoreCase(input))
            .findFirst()
            .orElse(null);
        return daysOfWeek != null;
    }
}
