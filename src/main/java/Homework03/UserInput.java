package Homework03;

import java.io.InputStream;
import java.util.Scanner;

import static Homework03.Utils.isDayOfWeekName;

public class UserInput {
    private final InputStream in = System.in;
    private final Scanner scanner = new Scanner(in);
    private UserInput() {
    }
    public static UserInput of() {
        return new UserInput();
    }
    public Command readCommand() {
        String input = scanner.nextLine().trim();
        while (!validateInput(input)) {
            System.out.print("Sorry, I don't understand you, please try again.\n");
            input = scanner.nextLine();
        }
        Command command = new Command();
        if (isCommandToFinish(input)) {
            command = new Command(Action.EXIT, command.dayOfWeek());
        }
        if (isCommandGetTasks(input)) {
            command = new Command(Action.READ_TASKS, DayOfWeek.valueOf(input.toUpperCase()));
        }
        if (isCommandUpdateTasks(input)) {
            String dayName = input.split(" ")[1].toUpperCase();
            command = new Command(Action.CHANGE, DayOfWeek.valueOf(dayName));
        }
        return command;
    }
    public String readNewTasks() {
        return scanner.nextLine().trim();
    }
    private boolean isCommandToFinish(String input) {
        return Action.EXIT.name().equalsIgnoreCase(input);
    }
    private boolean isCommandGetTasks(String input) { return isDayOfWeekName(input); }
    private boolean isCommandUpdateTasks(String input) {
        String [] inputArgs = input.split(" ");
        return inputArgs.length == 2
            && Action.CHANGE.name().equalsIgnoreCase(inputArgs[0])
            && isDayOfWeekName(inputArgs[1]);
    }
    private boolean validateInput(String input) {
        String[] inputArgs = input.split(" ");
        if (Action.EXIT.name().equalsIgnoreCase(input)) return true;
        if (isDayOfWeekName(input)) return true;
        return inputArgs.length == 2
            && Action.CHANGE.name().equalsIgnoreCase(inputArgs[0])
            && isDayOfWeekName(inputArgs[1]);
    }
}
