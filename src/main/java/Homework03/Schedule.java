package Homework03;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

import static Homework03.Data.dayOfWeekKey;
import static Homework03.Data.tasksKey;

public class Schedule {
    private final Store store;
    private Schedule(Store _store) {
        store = _store;
    }
    public static Schedule of(Store store) {
        return new Schedule(store);
    }
    public static String[][] initialize(String[] tasks) {
        AtomicInteger taskIdx = new AtomicInteger(0);
        return Arrays.stream(DayOfWeek.values())
            .map(dayOfWeek -> new String[]{dayOfWeek.toString(), tasks[taskIdx.getAndIncrement()]})
            .toArray(String[][]::new);
    }
    public String getDayTasks(DayOfWeek dayOfWeek) {
        String[][] scheduleItems = store.getState().scheduleItems();
        String[] scheduleItem = Arrays.stream(scheduleItems)
            .filter(item -> item[dayOfWeekKey] == dayOfWeek.toString())
            .findFirst()
            .orElse(null);
        return scheduleItem != null ? scheduleItem[tasksKey] : "";
    }
    public void updateDayTasks(DayOfWeek dayOfWeek, String tasks) {
        String[][] scheduleItems = store.getState().scheduleItems();
        String[][] updatedSchedule = Arrays.stream(scheduleItems)
            .map(scheduleItem -> scheduleItem[dayOfWeekKey] != dayOfWeek.toString()
                ? scheduleItem
                : new String[]{scheduleItem[dayOfWeekKey], tasks})
            .toArray(String[][]::new);
        store.updateSchedule(updatedSchedule);
    }
}
