package Homework03;

public class Store {
    private State state = new State(
        Schedule.initialize(Data.initialTasks)
    );
    private Store() {
    }
    public static Store of() {
        return new Store();
    }
    public State getState() {
        return state;
    }
    public void updateSchedule(String[][] scheduleItems) {
        state = new State(
            scheduleItems
        );
    }
}
