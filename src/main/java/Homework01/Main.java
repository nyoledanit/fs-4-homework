package Homework01;
import java.io.InputStream;
import java.util.Scanner;

public class Main {
    public static int random(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }
    public static boolean validateInt (String n) {
        try {
            int num = Integer.parseInt(n);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }
    public static boolean validateRange (String n, int min, int max) {
        int num = Integer.parseInt(n);
        return num >= min && num <= max;
    }

    public static boolean validateIntInRange(String n, int min, int max) {
        return validateInt(n) && validateRange(n, min, max);
    }
    public static int readInt() {
        InputStream in = System.in;
        Scanner scanner = new Scanner(in);
        String input = scanner.nextLine();;
        while (!validateIntInRange(input, 1, 100)) {
            System.out.println("Entered value is invalid. Try again, enter number between 1 and 100!");
            input = scanner.nextLine();
        }
        return Integer.parseInt(input);
    }
    public static void main(String[] args) {
        int randomNum = random(1, 100);
        int i = 0;
        InputStream in = System.in;
        Scanner scanner = new Scanner(in);
        System.out.printf("Enter your name %d\n", randomNum);
        String name = scanner.nextLine();
        System.out.println("Let the game begin!");
        System.out.println("Enter number!");
        String answerString = "";
        while (i != randomNum) {
            i = readInt();
            if (i < randomNum) {
                System.out.println("Your number is too small. Please, try again.");
            }
            if (i > randomNum) {
                System.out.println("Your number is too big. Please, try again.");
            }
        }
        System.out.printf("Congratulations, %s!", name);
    }
}